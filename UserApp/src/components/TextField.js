import React from 'react'

const TextField = (props) => {
	return (
		<form onSubmit={props.addTask}>
		  <input type="text" 
		    value={props.currentTask}
		    onChange={props.updateTask}
		   />
		   <button type="submit">Přidej</button>		
		</form>
	)
}

TextField.propTypes = {
	currentTask:React.PropTypes.string.isRequired,
	updateTask:React.PropTypes.func.isRequired,
	addTask:React.PropTypes.func.isRequired,
}

export default TextField