import React, {Component} from 'react';

const TableHeader = () => { 
    return (
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
    );
}

const TableBody = props => { 
    const rows = props.details.map((row, index) => {
        return (
            row.isEditing ? <TableRowEdit updateRow={props.updateRow} toggleState={props.toggleState} row={row} index={index} isEditing={row.isEditing} setCurrentTask={props.setCurrentTask}/>: 
                            <TableRow deleteTask={props.deleteTask} toggleState={props.toggleState} row={row} index={index} setIndex={props.setIndex}/>
        );
    });

    return <tbody>{rows}</tbody>;
}

const TableRowEdit = props => { 
    return (
        <tr key={props.index}>
            <td><input type="text" onChange={props.setCurrentTask} defaultValue={props.row.name} /></td>
            <td><button type="button" onClick={props.updateRow}>Update Item</button></td>
            {
            props.isEditing ? <td><button onClick={() => props.toggleState(props.index)}>Cancel</button></td> : null
            }
        </tr>
    );
}

const TableRow = props => { 
    return (
        <tr key={props.index}>
            <td>{props.row.name}</td>
            <td><button onClick={() => props.deleteTask(props.index)}>Delete</button></td>
            <td><button onClick={() => {props.toggleState(props.index);props.setIndex(props.index)}}>Edit</button></td>
        </tr>
    );
}

class Table extends Component {
    constructor(props){
        super(props);
        this.state = {
            editIndex:'',
            currentTask:'' 
          }
    
        this.updateRow = this.updateRow.bind(this);
        this.setIndex = this.setIndex.bind(this);
        this.setCurrentTask = this.setCurrentTask.bind(this);
    }
    
    updateRow(evt){
        evt.preventDefault();
        let editIndex = this.state.editIndex;
        let currentTask = this.state.currentTask;
        this.props.editTask(editIndex, currentTask);
        this.props.toggleState(editIndex);
    }

    setCurrentTask(newValue){
        this.setState({
            currentTask:newValue.target.value
        })
    }

    setIndex(index){
        this.setState({
            editIndex:index
        })
    }

    render() {
    const { deleteTask, details, toggleState} = this.props;
        return (
                <table>
                    <TableHeader />
                    
                    <TableBody deleteTask={deleteTask} 
                            details={details} 
                            toggleState={toggleState}
                            setIndex={this.setIndex}
                            setCurrentTask={this.setCurrentTask}
                            updateRow={this.updateRow}
                            />                                          
                </table>
        );
    }
}

export default Table;