import React from 'react';
import ReactDOM from 'react-dom'
import TextField from './components/TextField.js';
import Table from './components/Table.js';

class App extends React.Component {
    constructor(){
        super();
        this.updateTask = this.updateTask.bind(this);
        this.addTask = this.addTask.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.editTask = this.editTask.bind(this);
        this.toggleState = this.toggleState.bind(this);
        this.state = {
            tasks:[{
                name:"one",
                isEditing:false
                },
                {
                name:"Two",
                isEditing:false
                },
                {
                name:"Three",
                isEditing:false
                }],
            currentTask:'' 
        }
    }

    toggleState(index){
        var tasks = this.state.tasks;
        var task = tasks[index];
        task['isEditing'] = !task.isEditing;
        this.setState({
            tasks
        })
    }

    deleteTask(index){
        let tasks = this.state.tasks;
        tasks.splice(index,1);

        this.setState({
            tasks
        })
    }

    addTask(evt){
        evt.preventDefault();
        let tasks = this.state.tasks;
        let currentTask = this.state.currentTask;
        tasks.push({
            name:currentTask,
            isEditing:false
        })

        this.setState({
            tasks,
            currentTask:'' 
        })
    }

    updateTask(newValue){
        this.setState({
            currentTask:newValue.target.value
        })
    }

    editTask(index, newValue){
        var tasks = this.state.tasks;
        var task = tasks[index];
        task['name'] = newValue;
        this.setState({
            tasks
        })
    }

    render() {
        return (
            <section>
            <TextField
                currentTask={this.state.currentTask}
                updateTask={this.updateTask}
                addTask={this.addTask}
            />
              
            <Table deleteTask={this.deleteTask} 
                   editTask={this.editTask} 
                   details={this.state.tasks} 
                   toggleState={this.toggleState} />

            </section>
        )
    }
}

export default App;

